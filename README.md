# ReSpark Client Portal
This repository serves to show an example implementation of the ReSpark API for a customer client portal web app using React/Meteor. Use of this code base is intended to serve as an example on how you can integrate with ReSpark for your insurance program. Use of this code base is at your own risk.

## Demo
[Live Demo](https://client-demo.respark.insure)

## Setup
To get started you need:

* Meteor installed locally on your system ([Install Meteor](https://www.meteor.com/install))
* A ReSpark account ([Get ReSpark](https://respark.insure/))
* A Stripe account ([More information on Stripe](https://stripe.com/))

Next clone this repo:
 `git clone git@bitbucket.org:bematechnologies/respark-client-portal.git`

Then setup the correct config. You should make a `settings-development.json` file in the root of the repository which will be used for run time variables when running. Reference the `settings-example.json` file which looks like the example below:

```
{
  "private": {
    "RESPARK_SECRET_KEY": "sk_prod_....",
    "RESPARK_URL": "http://api.respark.insure",
    "RESPARK_PRODUCT_ID": "",
    "STRIPE_SECRET_KEY": "",
    "JWT_SIGNING_KEY": "something_secret!"
  },
  "public": {
    "STRIPE_PUBLIC_KEY": "",
    "CLIENT_TYPE": "person",
    "APP_NAME": "ReSpark Client Demo App"
  }
}
```

After this, in your terminal of choice, navigate to the root of the application and run:
`meteor npm install`

Lastly, to run locally:
`npm start`

## Developing Locally
Meteor allows a developer to work on the client and the server at the same time. In this paradigm, you need to be aware of what code is running where and how to safely configure your file structure to not expose client information. Please refer to [Meteor documentation](https://docs.meteor.com/) for more information. 

#### Working with Webhooks
To properly integrate with Stripe and ReSpark while developing locally, you will need to way for them to communicate events back to exposed endpoints. I recommend using a service called [ngrok](ngrok.io) to expose a tunnel to your localhost. To run simply use `ngrok http 3001` and connect webhook from Stripe and ReSpark accordingly.

## Stripe Setup
This portal is integrated to use Stripe's [customer portal](https://stripe.com/docs/billing/subscriptions/integrating-customer-portal). In order to make use of the client portals features for managing payment sources, subscriptions, and viewing invoices, make sure you [configure it in your account](https://stripe.com/docs/billing/subscriptions/integrating-customer-portal#configure).

## Deployment
Deploying our ReSpark client demo is just like deploying any other Meteor application. Please see Meteor's documentation on [Deployment and Monitoring](https://guide.meteor.com/deployment.html).

For a quick and easy Heroku Deployment please refer to [this heroku guide](https://medium.com/@leonardykris/how-to-run-a-meteor-js-application-on-heroku-in-10-steps-7aceb12de234).

1. Clone the project
2. Create a new project on Heroku (either using `heroku apps:create foobar`) or in the UI
3. Set the proper buildpack (either using `heroku buildpacks:set https://github.com/AdmitHub/meteor-buildpack-horse.git`) or in the UI
4. Set your Config Variables in Heroku. See image below as a guide:
![example heroku seeings](https://bitbucket.org/bematechnologies/respark-client-portal/src/demo/images/example-settings.png "Example Heroku Settings")
5. Go ahead and set your remote `heroku git:remote -a <app-name>`
6. Deploy to Heroku using `git push heroku master`

## Todo

* Finish integrating ReSpark webhooks.
* Finish integrating relevant Stripe webhooks.
* Support ReSpark Business client types
* Add documentation for AutoForm.
* Add Stripe webhook signing key support
* Add support for products outside the USA

## References

* [ReSpark API]([https://app.swaggerhub.com/apis-docs/Bema-Technologies/ReSpark/1.0.0](https://app.swaggerhub.com/apis-docs/Bema-Technologies/ReSpark/1.0.0))
* [Meteor Documentation](https://docs.meteor.com/)
* [Stripe API](https://stripe.com/docs/api)
* [ngrok](https://ngrok.com/)
