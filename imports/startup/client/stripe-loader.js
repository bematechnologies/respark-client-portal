import { loadStripe } from '@stripe/stripe-js';

// load stripe on startup
window.stripePromise = loadStripe(Meteor.settings.public.STRIPE_PUBLIC_KEY);