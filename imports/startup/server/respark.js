import { HTTP } from 'meteor/http';

// simple wrapper for the ReSpark REST API

ReSpark = {};

const call = (endpoint, method, options) => {
  try {
    let res = HTTP.call(method, `${Meteor.settings.private.RESPARK_URL}/api/v1${endpoint}`, {
      headers: {
        "x-api-key": Meteor.settings.private.RESPARK_SECRET_KEY
      },
      ...options
    });

    return res;
  } catch (e) {
    console.log(e);
    return e;
  }
}

ReSpark.call = call;