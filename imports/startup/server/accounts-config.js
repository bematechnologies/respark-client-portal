import { Accounts } from 'meteor/accounts-base';

// simple config for accounts on Meteor
// https://docs.meteor.com/api/accounts-multi.html#AccountsCommon-config

Accounts.config({
  forbidClientAccountCreation: true
});

// Deny all client-side updates to user documents
Meteor.users.deny({
  update() { return true; }
});
