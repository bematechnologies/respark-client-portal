import { WebApp } from 'meteor/webapp';
import bodyParser from 'body-parser';

// all our webhooks expect json encoded bodies
// let's use body parser for this

const rawParser = bodyParser.json();
WebApp.connectHandlers.use(rawParser);
