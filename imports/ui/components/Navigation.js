import React from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';

import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap';
import { useTracker } from 'meteor/react-meteor-data';

const Navigation = () => {
  const user = useTracker(() => Meteor.user(), []);

  if (user) {
    return (
      <Navbar bg="light" expand="lg">
        <Container>
          <LinkContainer to='/home'>
            <Navbar.Brand>
              {Meteor.settings.public.APP_NAME}
          </Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <LinkContainer to='/home'>
                <Nav.Link>Home</Nav.Link>
              </LinkContainer>
              <LinkContainer to='/account'>
                <Nav.Link>Account</Nav.Link>
              </LinkContainer>
              <Nav.Link onClick={() => Meteor.logout()}>Logout</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    )
  }

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <IndexLinkContainer to='/'>
          <Navbar.Brand>
            {Meteor.settings.public.APP_NAME}
        </Navbar.Brand>
        </IndexLinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <LinkContainer to='/login'>
              <Nav.Link>Login</Nav.Link>
            </LinkContainer>
            <LinkContainer to='/signup'>
              <Nav.Link>Get Started</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Navigation;