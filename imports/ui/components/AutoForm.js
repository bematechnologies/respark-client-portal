import { withTracker } from 'meteor/react-meteor-data';

import React from 'react';
import PropTypes from 'prop-types';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap-button-loader';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import InputGroup from 'react-bootstrap/InputGroup';
import InputNumber from 'rc-input-number';

import { Formik } from 'formik';
import * as Yup from 'yup';

class AutoForm extends React.Component {
  handleFormSubmit = (formikProps) => {
    const { disabled } = this.props;

    if (disabled) {
      return;
    }

    if (!formikProps.isSubmitting) {
      formikProps.handleSubmit();
    }
  }

  renderSelect = ({ element, values, handleChange, handleBlur, disabled }) => {
    if (!element.sizing) {
      element.sizing = {};
      element.sizing.xs = '12'
    }

    const formikHandleChange = handleChange;
    const onChange = (e) => {
      formikHandleChange(e);
      if (element.attributes.onChange) {
        element.attributes.onChange(e)
      }
    }

    // note we always add a label even if one isn't provided per best aria practices
    return (
      <Col key={element.key} {...element.sizing}>
        <Form.Group>
          {element.label && <Form.Label aria-label={element.ariaLabel || element.label} id={`${element.key}-label`}>{element.label}</Form.Label>}
          {!element.label && <Form.Label className='d-none' aria-label={element.ariaLabel || element.key} />}
          <Form.Control
            {...element.attributes}
            as={element.attributes.type}
            value={values[element.attributes.name]}
            onBlur={handleBlur}
            onChange={onChange}
            disabled={disabled}
            aria-labelledby={`${element.key}-label`}
          >
            {element.options.map((option, i) => {
              return (
                <option key={`option-${i}`} value={option.value}>{option.display}</option>
              );
            })}
          </Form.Control>
          {element.formText && <Form.Text>{element.formText}</Form.Text>}
        </Form.Group>
      </Col>
    );
  }

  renderRadio = ({ element, handleChange, handleBlur, disabled, values }) => {
    if (!element.sizing) {
      element.sizing = {};
      element.sizing.xs = '12'
    }

    return (
      <Col key={element.key} {...element.sizing}>
        <Form.Group>
          <Form.Label aria-label={element.ariaLabel || element.label} id={`${element.key}-label`}>{element.label}</Form.Label>
          {element.options.map(option => {
            return (
              <Form.Group key={option.value} aria-labelledby={`${element.key}-label`}>
                <Form.Label className='custom-control custom-radio' aria-label={option.display} id={`radio-option-${option.value}`}>
                  <Form.Control
                    {...element.attributes}
                    type={element.attributes.type}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    disabled={disabled}
                    aria-labelledby={`radio-option-${option.value}`}
                    aria-checked={values[element.attributes.name] == option.value ? "true" : "false"}
                    className='custom-control-input' />
                  {' '}
                  <span className="custom-control-indicator" />
                  <span className="custom-control-description">{option.display}</span>
                </Form.Label>
              </Form.Group>
            );
          })}
          {element.formText && <Form.Text aria-label={element.formText}>{element.formText}</Form.Text>}
        </Form.Group>
      </Col>
    )
  }

  renderCheckbox = ({ element, handleChange, handleBlur, disabled, values }) => {
    if (!element.sizing) {
      element.sizing = {};
      element.sizing.xs = '12'
    }

    const formikHandleChange = handleChange;
    const onChange = (e) => {
      formikHandleChange(e);

      if (element.attributes.onChange) {
        element.attributes.onChange(e)
      }
    }

    return (
      <Col key={element.key} {...element.sizing}>
        <Form.Group className='pt-2'>
          <Form.Label className="custom-control custom-checkbox" aria-label={element.ariaLabel || element.label} id={`${element.key}-label`}>
            <Form.Control
              {...element.attributes}
              type={element.attributes.type}
              onBlur={handleBlur}
              onChange={onChange}
              disabled={disabled}
              className="custom-control-input"
              aria-labelledby={`${element.key}-label`}
              aria-checked={values[element.attributes.name] ? "true" : "false"} />
            <span className="custom-control-indicator" />
            <span className="custom-control-description" style={{ cursor: 'pointer', fontWeight: 'normal' }}></span>
            {element.label}
          </Form.Label>
          {element.formText && <Form.Text aria-label={element.formText}>{element.formText}</Form.Text>}
        </Form.Group>
      </Col>
    )
  }

  renderGeneric = ({ element, values, handleChange, handleBlur, errors, touched, disabled, setFieldValue }) => {
    if (!element.sizing) {
      element.sizing = {};
      element.sizing.xs = '12'
    }

    if (element.attributes.type == 'textarea') {
      element.attributes.as = 'textarea';
    }

    const formikHandleChange = handleChange;
    const onChange = (e) => {
      formikHandleChange(e);

      if (element.attributes.onChange) {
        element.attributes.onChange(e, setFieldValue);
      }
    }

    if (element.prependInput || element.appendInput) {
      // note we always add a label even if one isn't provided per best aria practices
      return (
        <Col key={element.key} {...element.sizing}>
          <Form.Group>
            {element.label && <Form.Label aria-label={element.ariaLabel || element.label} id={`${element.key}-label`}>{element.label}</Form.Label>}
            {!element.label && <Form.Label className='d-none' aria-label={element.ariaLabel || element.key} />}
            <InputGroup>
              {element.prependInput && <InputGroup.Prepend><InputGroup.Text>{element.prependInput}</InputGroup.Text></InputGroup.Prepend>}
              {(element.attributes.type != 'number' && element.attributes.type != 'money') && <Form.Control
                {...element.attributes}
                type={element.attributes.type || 'text'}
                value={values[element.attributes.name]}
                onBlur={handleBlur}
                onChange={onChange}
                disabled={disabled || element.attributes.disabled}
                isInvalid={errors[element.attributes.name] && touched[element.attributes.name]}
                aria-required={element.attributes.required}
                aria-labelledby={`${element.key}-label`}
              />}
              {(element.attributes.type == 'number' || element.attributes.type == 'money') && <InputNumber
                {...element.attributes}
                formatter={value => element.attributes.type == "number" ? value : `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                prefixCls='form-control'
                style={{ padding: "0px" }}
                type={element.attributes.type || 'text'}
                value={values[element.attributes.name]}
                onBlur={handleBlur}
                onChange={(value) => {
                  setFieldValue(element.attributes.name, value);

                  if (element.attributes.onChange) {
                    let e = {
                      target: {
                        value: value
                      }
                    }

                    element.attributes.onChange(e)
                  }
                }}
                disabled={disabled || element.attributes.disabled}
                isInvalid={errors[element.attributes.name] && touched[element.attributes.name]}
                aria-required={element.attributes.required}
                aria-labelledby={`${element.key}-label`}
              />}
              {element.appendInput && <InputGroup.Append><InputGroup.Text>{element.appendInput}</InputGroup.Text></InputGroup.Append>}
            </InputGroup>
            {element.formText && <Form.Text aria-label={element.formText}>{element.formText}</Form.Text>}
            {errors[element.attributes.name]
              && touched[element.attributes.name] && <FormControl.Feedback type="invalid" aria-label={errors[element.attributes.name]}>{errors[element.attributes.name]}</FormControl.Feedback>}
          </Form.Group>
        </Col>
      )
    }

    // note we always add a label even if one isn't provided per best aria practices
    return (
      <Col key={element.key} {...element.sizing}>
        <Form.Group>
          {element.label && <Form.Label aria-label={element.ariaLabel || element.label} id={`${element.key}-label`}>{element.label}</Form.Label>}
          {!element.label && <Form.Label className='d-none' aria-label={element.ariaLabel || element.key} />}
          {!element.label && <Form.Label className='d-none' aria-label={element.ariaLabel || element.key} />}
          {(element.attributes.type != 'number' && element.attributes.type != 'money') && <Form.Control
            {...element.attributes}
            type={element.attributes.type || 'text'}
            value={values[element.attributes.name]}
            onBlur={handleBlur}
            onChange={onChange}
            disabled={disabled || element.attributes.disabled}
            isInvalid={errors[element.attributes.name] && touched[element.attributes.name]}
            aria-required={element.attributes.required}
            aria-labelledby={`${element.key}-label`}
          />}
          {(element.attributes.type == 'number' || element.attributes.type == 'money') && <InputNumber
            {...element.attributes}
            formatter={value => element.attributes.type == "number" ? value : `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            prefixCls='form-control'
            style={{ padding: "0px" }}
            type={element.attributes.type || 'text'}
            value={values[element.attributes.name]}
            onBlur={handleBlur}
            onChange={(value) => {
              setFieldValue(element.attributes.name, value);

              if (element.attributes.onChange) {
                let e = {
                  target: {
                    value: value
                  }
                }

                element.attributes.onChange(e)
              }
            }}
            disabled={disabled || element.attributes.disabled}
            isInvalid={errors[element.attributes.name] && touched[element.attributes.name]}
            aria-required={element.attributes.required}
            aria-labelledby={`${element.key}-label`}
        />}
          {element.formText && <Form.Text aria-label={element.formText}>{element.formText}</Form.Text>}
          {errors[element.attributes.name]
            && touched[element.attributes.name] && <FormControl.Feedback type="invalid" aria-label={errors[element.attributes.name]}>{errors[element.attributes.name]}</FormControl.Feedback>}
        </Form.Group>
      </Col>
    )
  }

  renderFields = (formikProps) => {
    const { schema, disabled } = this.props;
    let inputElements = [];

    schema.forEach(element => {
      switch (element.attributes.type) {
        case 'select':
          inputElements.push(this.renderSelect({ element, disabled, ...formikProps }));
          break;
        case 'radio':
          inputElements.push(this.renderRadio({ element, disabled, ...formikProps }));
          break;
        case 'checkbox':
          inputElements.push(this.renderCheckbox({ element, disabled, ...formikProps }));
          break;
        default:
          inputElements.push(this.renderGeneric({ element, disabled, ...formikProps }));
      }
    });

    return inputElements;
  }

  gatherInitialValues = () => {
    const { schema } = this.props;
    let initialValues = {};

    schema.forEach(field => {
      if (field.attributes.value != undefined) {
        initialValues[field.attributes.name] = field.attributes.value;
      } else {
        initialValues[field.attributes.name] = '';
      }
    });

    return initialValues;
  }

  gatherYupCriteria = () => {
    const { schema } = this.props;
    let yupCriteria = {};

    schema.forEach(field => {
      let validationMethod = null;

      let type = field.attributes.type;

      // use the validation method passed in if one has been defined
      // we only validate on known types
      if (field.validationMethod) {
        validationMethod = field.validationMethod;
      } else if (type && (type == 'number' || type == 'text' || type == 'password' || type == 'textarea' || type == 'email')) {

        // start with types
        if (type == 'number') {
          validationMethod = Yup.number();
        } else {
          validationMethod = Yup.string();
        }

        // required
        if (field.attributes.required) {
          validationMethod = validationMethod.required('Required');
        }

        // email
        if (type == 'email') {
          validationMethod = validationMethod.email('Invalid email address');
        }

        // min
        if (field.attributes.min) {
          validationMethod = validationMethod.min(field.attributes.min, `A minimum of ${field.attributes.min}${type == 'number' ? '' : ' characters'} is required`)
        }

        // max
        if (field.attributes.max) {
          validationMethod = validationMethod.max(field.attributes.max, `Only a max value of ${field.attributes.max}${type == 'number' ? '' : ' characters'} is accepted`)
        }

        // url
        if (field.attributes.url) {
          validationMethod = validationMethod.url('Invalid URL address. A valid URL started with http://');
        }
      }

      // add in the validation method if it has been set
      if (validationMethod) {
        yupCriteria[field.attributes.name] = validationMethod;
      }
    });

    return yupCriteria;
  }

  renderSaveButton({ isSubmitting }) {
    const { button, hideSubmitButton, disabled } = this.props;

    return (
      <Col xs='12' style={button.wrapperStyles} >
        <Button
          className={button.className || null}
          id={button.id || null}
          loading={isSubmitting}
          style={button.buttonStyles}
          type="submit"
          varient={button.variant}
          hidden={hideSubmitButton}
          disabled={disabled}
          size={button.size || 'md'}
          aria-label={`submit form`}>
          {button.text}
        </Button>
      </Col>
    );
  }

  render() {
    const { style, handleSubmit, container, submitForm, id } = this.props;

    return (
      <div style={style}>
        <Formik
          initialValues={this.gatherInitialValues()}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            handleSubmit(values, setSubmitting, resetForm);
          }}
          validationSchema={Yup.object().shape(this.gatherYupCriteria())}
        >
          {(formikProps) => {
            if (submitForm) {
              submitForm(formikProps.submitForm);
            }

            return (
              <form id={id || null} onSubmit={(e) => {
                e.preventDefault();

                this.handleFormSubmit(formikProps)
              }}>
                <Container style={container.styles}>
                  <Row>
                    {this.renderFields(formikProps)}
                    {this.renderSaveButton(formikProps)}
                  </Row>
                </Container>
              </form>
            )
          }}
        </Formik>
      </div>
    )
  }
}

AutoForm.defaultProps = {
  style: {},
  button: {
    wrapperStyles: {},
    buttonStyles: {},
    variant: "primary",
    text: "Submit"
  },
  container: {
    styles: {}
  }
}

AutoForm.propTypes = {
  schema: PropTypes.arrayOf(PropTypes.shape({
    attributes: PropTypes.shape({
      type: PropTypes.string,
      required: PropTypes.bool,
      name: PropTypes.string,
      id: PropTypes.string,
      placeholder: PropTypes.string,
      min: PropTypes.number,
      max: PropTypes.number,
      disabled: PropTypes.bool,
      autoFocus: PropTypes.bool
    }),
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    key: PropTypes.string,
    validationMethod: PropTypes.object,
    sizing: PropTypes.shape({
      xs: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      sm: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      lg: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      xl: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    }),
    prependInput: PropTypes.string
  })).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  button: PropTypes.shape({
    id: PropTypes.string,
    wrapperStyles: PropTypes.object,
    buttonStyles: PropTypes.object,
    variant: PropTypes.string,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    size: PropTypes.string,
    className: PropTypes.string
  }),
  container: PropTypes.shape({
    styles: PropTypes.object
  }),
  style: PropTypes.object,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  submitForm: PropTypes.func
};

export default withTracker((props) => {
  let schema = props.schema;

  // cleans DOM types that are not supported
  // just converts boolean types to true or false for autoform support
  let cleanedSchema = schema.map(element => {
    if (typeof element.attributes.url == "boolean") {
      element.attributes.url = element.attributes.url ? "true" : "false";
    }

    return element;
  });

  return {
    schema: cleanedSchema
  }
})(AutoForm);