import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useTracker } from 'meteor/react-meteor-data'

const CustomRoute = ({ exact, path, component, public, authenticated }) => {
  const user = useTracker(() => Meteor.user(), []);
  const loggingIn = useTracker(() => Meteor.loggingIn(), []);

  if (loggingIn && path != '/' && path != '/login') {
    return null;
  }

  if (user && public) {
    return <Redirect to='/home' />
  }

  if (!user && authenticated) {
    return <Redirect to='/login' />
  }

  return (
    <Route exact={exact} path={path} component={component} />
  )
}

export default CustomRoute;