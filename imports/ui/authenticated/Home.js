import React from 'react';
import { Random } from 'meteor/random';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

import JSZip from 'jszip';
import slugify from 'slugify';
import moment from 'moment';

class Home extends React.Component {
  state = {
    isLoading: true,
    policies: []
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    Meteor.call("policies.getInformation", (err, res) => {
      let update = { isLoading: false };

      if (res) {
        update = { ...update, ...res };
      }

      this.setState(update);
    });
  }

  handleDownloadDocuments = (policyId) => {
    Meteor.call("policy.getDocuments", policyId, async (err, res) => {
      if (res && Array.isArray(res)) {
        let zip = new JSZip();
        let finishedDownloads = 0;

        res.forEach((token, i) => {
          let title = "policy-document";
          fetch(`/documents`, {
            method: "GET",
            headers: new Headers({
              'x-token': token
            })
          })
            .then((response) => {
              title = response.headers.get("x-title");
              return response.blob();
            })
            .then((blob) => {
              zip.file(`${slugify(title)}-${Random.id(4)}.pdf`, blob);

              finishedDownloads += 1;
              if (finishedDownloads == res.length) {
                zip.generateAsync({ type: "blob" })
                  .then((content) => {
                    var url = window.URL.createObjectURL(content);
                    var a = document.createElement('a');
                    a.href = url;
                    a.download = `Policy-Files.zip`;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                  })
              }
            });
        })
      }
    });
  }

  renderPolicies() {
    const { policies } = this.state;

    return policies.map((policy, i) => (
      <div className='basic-box p-3' key={`policy-${i}`}>
        <div className='mb-2'>Policy Number: <strong>{`#${policy.policyNumber}`}</strong></div>
        <div className='mb-2'>{`Status: ${policy.status}`}</div>
        <div className='mb-2'>{`Name Insured: ${policy.name}`}</div>
        {policy.status == "in-force" && <div className='mb-2'>{`End Date: ${moment(policy.termEndDate).format('LL')}`}</div>}
        <div><a href='#' onClick={() => this.handleDownloadDocuments(policy.policyId)}>View Documents</a></div>
      </div>
    ));
  }

  renderClaims() {
    const { claims } = this.state;

    if (claims.length == 0) {
      return (
        <div className='basic-box p-3'>
          No claims found.
        </div>
      )
    }

    return claims.map((claim, i) => (
      <div className='basic-box p-3' key={`claim-${i}`}>
        <div className='mb-2'>{`Created at: ${moment(claim.createdAt).format("LL")}`}</div>
        <div>{`Status: ${claim.status}`}</div>
      </div>
    ))
  }

  render() {
    const { isLoading } = this.state;

    if (isLoading) {
      return (
        <div className='normal-padding text-center'>
          <Spinner animation="border" />
        </div>
      );
    }

    return (
      <div className='normal-padding' >
        <Container>
          <Row>
            <Col xs='12'>
              <h5 className='pb-2'>Your Policies</h5>
              {this.renderPolicies()}
              <h5 className='pb-2'>Your Claims</h5>
              {this.renderClaims()}
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Home;