import React from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import AutoForm from '../components/AutoForm';

import * as Yup from 'yup';

import { Accounts } from 'meteor/accounts-base';
import { withTracker } from 'meteor/react-meteor-data';

import toastAlert from '../../modules/client/toast-alert';
import { Button } from 'react-bootstrap';
import Icon from '../components/Icon';

class Account extends React.Component {
  handleUpdateEmail = (values, setSubmitting) => {
    Meteor.call("users.changeEmail", values.email, (err) => {
      if (err) {
        toastAlert("danger", err.reason);
      } else {
        toastAlert("success", "Email address updated");
      }

      setSubmitting(false);
    })
  }

  handleUpdatePassword = (values, setSubmitting, resetForm) => {
    Accounts.changePassword(values.password, values.newPassword, (err) => {
      if (err) {
        toastAlert("danger", err.reason);
      } else {
        toastAlert("success", "Password changed");
        resetForm();
      }

      setSubmitting(false);
    });
  }

  handleRedirectToBilling = () => {
    Meteor.call('stripe.getCustomerPortalToken', (err, res) => {
      if (res) {
        window.location.assign(res);
      }
    })
  }

  render() {
    const { email } = this.props;

    if (!email) {
      return null;
    }

    return (
      <div className='normal-padding'>
        <Container>
          <Row>
            <Col xs='12'>
              <h5 className='mb-3'>Update Email Address</h5>
              <div className='basic-box pt-3 pb-3'>
                <AutoForm
                  handleSubmit={this.handleUpdateEmail}
                  button={{
                    text: "Save",
                    wrapperStyles: {
                      textAlign: "right"
                    },
                    buttonStyles: {
                      width: "150px"
                    }
                  }}
                  schema={[
                    {
                      attributes: {
                        name: "email",
                        type: "email",
                        placeholder: "Email Address",
                        value: email
                      },
                      key: "email",
                      label: "Email Address",
                    },
                  ]}
                />
              </div>
              <h5 className='mb-3'>Update Password</h5>
              <div className='basic-box pt-3 pb-3'>
                <AutoForm
                  handleSubmit={this.handleUpdatePassword}
                  button={{
                    text: "Save",
                    wrapperStyles: {
                      textAlign: "right"
                    },
                    buttonStyles: {
                      width: "150px"
                    }
                  }}
                  schema={[
                    {
                      attributes: {
                        name: "password",
                        type: "password",
                        max: 256,
                        placeholder: "Password",
                        required: true,
                        autoComplete: "current-password"
                      },
                      key: "password",
                      label: "Password",
                    },
                    {
                      attributes: {
                        name: "newPassword",
                        type: "password",
                        max: 256,
                        placeholder: "New Password",
                        required: true,
                        autoComplete: "new-password"
                      },
                      key: "newPassword",
                      label: "New Password",
                      validationMethod: Yup.string().required("Required").min(8, 'Minimum of 8 character').max(128, 'Maximum of 128 characters')
                    },
                    {
                      attributes: {
                        name: "confirmPassword",
                        type: "password",
                        placeholder: "Confirm New Password",
                        required: true,
                        autoComplete: "new-password"
                      },
                      key: "confirmPassword",
                      label: "Confirm New Password",
                      max: 256,
                      validationMethod: Yup.string().required("Required").oneOf([Yup.ref('newPassword'), null], 'Your passwords do not match')
                    }
                  ]}
                />
              </div>
            </Col>
            <Col xs='auto' className=' ml-auto mt-3'>
              <Button onClick={this.handleRedirectToBilling} size='lg'>
                Billing Preferences<Icon className='ml-2' icon='external-link-alt' />
              </Button>
            </Col>
          </Row>
        </Container>
      </div >
    )
  }
}

export default withTracker(() => {
  let user = Meteor.user();
  let email = null;
  if (user && user.emails && user.emails[0] && user.emails[0].address) {
    email = user.emails[0].address;
  }

  return {
    email
  }
})(Account);