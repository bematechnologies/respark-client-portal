import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';
import AutoForm from '../components/AutoForm';

import * as Yup from 'yup';
import toastAlert from '../../modules/client/toast-alert';

const Login = () => {
  const handleSubmit = (values, setSubmitting) => {
    Meteor.loginWithPassword(values.email, values.password, (err) => {
      if (err) {
        toastAlert("danger", err.reason);
        setSubmitting(false);
      }
    });
  }

  return (
    <div className='normal-padding'>
      <AutoForm
        handleSubmit={handleSubmit}
        button={{
          text: "Login",
          size: "lg",
          wrapperStyles: {
            textAlign: "center"
          },
          buttonStyles: {
            marginTop: "8px",
            width: "150px"
          }
        }}
        schema={[
          {
            attributes: {
              name: "email",
              type: "email",
              placeholder: "Email Address"
            },
            key: "email",
            label: "Email Address",
            sizing: {
              xs: 12,
              sm: 12,
              md: {
                span: 8,
                offset: 2
              },
              lg: {
                span: 6,
                offset: 3
              }
            }
          },
          {
            attributes: {
              name: "password",
              type: "password",
              max: 256,
              placeholder: "Password",
              required: true,
              autoComplete: "current-password"
            },
            key: "password",
            label: "Password",
            validationMethod: Yup.string().required("Required").min(8, 'Minimum of 8 character').max(128, 'Maximum of 128 characters'),
            sizing: {
              xs: 12,
              sm: 12,
              md: {
                span: 8,
                offset: 2
              },
              lg: {
                span: 6,
                offset: 3
              }
            }
          }
        ]}
      />
      <Container>
        <Row>
          <Col xs='12' className='text-center pt-4'>
            Don't have an account? <Link to='/signup'>Get Started.</Link>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Login;