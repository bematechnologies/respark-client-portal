import React from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import AutoForm from '../components/AutoForm';
import { Redirect } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner';

import toastAlert from '../../modules/client/toast-alert';

class Signup extends React.Component {
  state = {
    isLoading: true,
    error: null,
    fields: [],
    locations: [],
    clientFormData: null,
    redirect: null
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    Meteor.call("signup.getProductInformation", false, (err, res) => {
      let update = { isLoading: false };
      if (err) {
        update.error = err.reason;
      } else {
        update = {
          ...update,
          ...res
        }
      }

      this.setState(update);
    })
  }

  handleSubmitClientForm = (values, setSubmitting) => {
    const { fields } = this.state;
    this.setState({
      clientFormData: values,
    }, () => {
      if (fields.length == 0) {
        this.handleCreateQuote({}, setSubmitting);
      } else {
        setSubmitting(false);
        this.props.history.push("/signup/additional-info")
      }
    })
  }

  handleCreateQuote = (values, setSubmitting) => {
    const { clientFormData } = this.state;

    Meteor.call('signup.createQuote', {
      ...clientFormData,
      additionalFields: {
        ...values
      }
    }, (err, res) => {
      setSubmitting(false);
      if (err) {
        toastAlert("danger", err.reason);
      } else if (!res) {
        toastAlert("danger", "There was a problem with your request");
      } else {
        this.setState({
          redirect: `/signup/create-account/${res}`
        });
      }
    })
  }

  renderPage() {
    const { page } = this.props.match.params;

    switch (page) {
      case 'additional-info':
        return this.renderProductForm();
      case 'get-started':
      default:
        return this.renderClientForm();
    }
  }

  renderProductForm() {
    if (!this.state.clientFormData) {
      return <Redirect to='/signup/get-started' />
    }

    return (
      <Container className='normal-padding' key="product-form">
        <Row className='align-items-center'>
          <Col xs='12' className='text-center'>
            <h2 className='mb-4'>{`Protect Your Fish from Drowning Today!`}</h2>
          </Col>
          <Col xs='12'>
            <AutoForm
              handleSubmit={this.handleCreateQuote}
              container={{
                styles: {
                  padding: "0px"
                }
              }}
              button={{
                text: "Get Quote",
                wrapperStyles: {
                  textAlign: "center",
                  paddingTop: "16px"
                },
                size: "lg",
                buttonStyles: {
                  width: "200px"
                }
              }}
              schema={this.state.fields}
            />
          </Col>
        </Row>
      </Container>
    )
  }

  renderClientForm() {
    const { fields, locations } = this.state;

    return (
      <Container className='normal-padding' key='client-form'>
        <Row>
          <Col xs='12' className='text-center'>
            <h2 className='mb-4'>{`Protect Your Fish from Drowning Today!`}</h2>
          </Col>
          <Col xs='12'>
            <AutoForm
              handleSubmit={this.handleSubmitClientForm}
              container={{
                styles: {
                  padding: "0px"
                }
              }}
              button={{
                text: fields.length == 0 ? "Get Quote" : "Next",
                wrapperStyles: {
                  textAlign: "center",
                  paddingTop: "16px"
                },
                size: "lg",
                buttonStyles: {
                  width: "200px"
                }
              }}
              schema={[
                {
                  attributes: {
                    type: "text",
                    name: "firstName",
                    placeholder: "First Name",
                    autoComplete: "given-name",
                    required: true,
                  },
                  label: "First Name",
                  key: "firstName",
                  sizing: {
                    xs: 12,
                    sm: 12,
                    md: 6
                  }
                },
                {
                  attributes: {
                    type: "text",
                    name: "lastName",
                    placeholder: "Last Name",
                    autoComplete: "given-name",
                    required: true,
                  },
                  label: "Last Name",
                  key: "lastName",
                  sizing: {
                    xs: 12,
                    sm: 12,
                    md: 6
                  }
                },
                {
                  attributes: {
                    type: "email",
                    name: "emailAddress",
                    placeholder: "Email Address",
                    autoComplete: "username",
                    required: true,
                  },
                  key: "emailAddress",
                  label: "Email Address"
                },
                {
                  attributes: {
                    type: "text",
                    name: "line1",
                    placeholder: "Address Line 1",
                    required: true,
                  },
                  key: "line1",
                  label: "Address Line 1"
                },
                {
                  attributes: {
                    type: "text",
                    name: "line2",
                    placeholder: "Address Line 2",
                  },
                  key: "line2",
                  label: "Address Line 2"
                },
                {
                  attributes: {
                    type: "text",
                    name: "city",
                    placeholder: "City",
                  },
                  key: "city",
                  label: "City",
                  sizing: {
                    xs: 12,
                    sm: 12,
                    md: 4
                  }
                },
                {
                  attributes: {
                    type: "select",
                    name: "state",
                    placeholder: "State",
                  },
                  key: "state",
                  label: "State",
                  sizing: {
                    xs: 12,
                    sm: 12,
                    md: 4
                  },
                  options: locations
                },
                {
                  attributes: {
                    type: "text",
                    name: "zip",
                    placeholder: "Zip",
                  },
                  key: "zip",
                  label: "Zip",
                  sizing: {
                    xs: 12,
                    sm: 12,
                    md: 4
                  }
                }
              ]}
            />
          </Col>
        </Row>
      </Container>
    )
  }

  render() {
    const { isLoading, error, redirect } = this.state;

    if (redirect) {
      return <Redirect to={redirect} />
    }

    if (isLoading) {
      return (
        <div className='normal-padding text-center'>
          <Spinner animation="border" />
        </div>
      )
    }

    if (error) {
      return (
        <Container className='normal-padding'>
          <Row>
            <Col xs='12'>
              <Alert variant='danger'>
                There was an error with your request.
              </Alert>
            </Col>
          </Row>
        </Container>
      )
    }

    return this.renderPage();
  }
}

export default Signup;