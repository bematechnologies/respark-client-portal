import React from 'react';

import { Accounts } from 'meteor/accounts-base';

import NotFound from '../../common/NotFound';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Icon from '../components/Icon';
import FormLabel from 'react-bootstrap/FormLabel';
import AutoForm from '../components/AutoForm';
import LoadingButton from 'react-bootstrap-button-loader';

import { ElementsConsumer, CardElement, Elements } from '@stripe/react-stripe-js';

import centsToDollars from '../../modules/common/cents-to-dollars';
import * as Yup from 'yup';
import toastAlert from '../../modules/client/toast-alert';

const QuoteSelect = ({ _id, limit, deductible, total, selectedQuote, onClick, currency }) => {
  let isChecked = selectedQuote && selectedQuote._id == _id;

  return (
    <ListGroup.Item action onClick={onClick} active={isChecked}>
      <Row className='align-items-center'>
        <Col xs='8'>
          <h6>{`Limit: ${centsToDollars(limit, currency.decimalDigits, currency.symbol)}, Deductible: ${centsToDollars(deductible, currency.decimalDigits, currency.symbol)}`}</h6>
          <div>{`Total: ${centsToDollars(total, currency.decimalDigits, currency.symbol)}`}</div>
        </Col>
        <Col xs='4' className='text-right'>
          <div className={`fa-check-box`}>
            {isChecked && <Icon size='2x' icon='check' />}
          </div>
        </Col>
      </Row>
    </ListGroup.Item>
  )
}

const PaymentPlanSelect = ({ months, selectedBillingInterval, onClick }) => {
  let isChecked = selectedBillingInterval && selectedBillingInterval == months;

  let label = "";
  let description = ""
  switch (months) {
    case 1:
      label = "Monthly";
      description = "You will be billed once a month."
      break;
    case 3:
      label = "Quarterly";
      description = "You will be billed every 3 months";
      break;
    case 6:
      label = "Semi-Annually";
      description = "You will be billed every 6 months";
      break;
    case 12:
    default:
      label = "Yearly";
      description = "You will be billed once a year."
      break;
  }

  return (
    <ListGroup.Item action onClick={onClick} active={isChecked}>
      <Row className='align-items-center'>
        <Col xs='8'>
          <h6>{label}</h6>
          <div>{description}</div>
        </Col>
        <Col xs='4' className='text-right'>
          <div className={`fa-check-box`}>
            {isChecked && <Icon size='2x' icon='check' />}
          </div>
        </Col>
      </Row>
    </ListGroup.Item>
  )
}

class SelectQuoteAndCreateAccount extends React.Component {
  state = {
    isLoading: true,
    quotes: [],
    selectedQuote: null,
    selectedBillingInterval: null,
    isSubmitting: false
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    Meteor.call("signup.getQuote", this.props.match.params.quoteGroupId, (err, res) => {
      let update = { isLoading: false };

      if (res) {
        update.quotes = res;
      }

      this.setState(update);
    });
  }

  handleSubmit = (values, setSubmitting) => {
    const { selectedBillingInterval, selectedQuote } = this.state;

    if (!selectedQuote) {
      toastAlert("danger", "Please select a plan");
      setSubmitting(false);
      return;
    }

    if (!selectedBillingInterval) {
      toastAlert("danger", "Please select a payment plan");
      setSubmitting(false);
      return;
    }

    const { stripe, elements } = this.props;
    this.setState({
      isSubmitting: true
    }, async () => {

      // disable submission until stripe loads
      if (!stripe || !elements) {
        setSubmitting(false);
        this.setState({ isSubmitting: false });
        return;
      }

      const cardElement = elements.getElement(CardElement);
      const { error, paymentMethod } = await stripe.createPaymentMethod({
        type: 'card',
        card: cardElement
      });

      if (error) {
        toastAlert("danger", error.message);
        setSubmitting(false);
        this.setState({ isSubmitting: false });
        return;
      }

      Meteor.call("signup.bindPolicy", {
        quoteGroupId: this.props.match.params.quoteGroupId,
        quoteId: selectedQuote._id,
        billingInterval: selectedBillingInterval,
        paymentMethodId: paymentMethod.id,
        password: Accounts._hashPassword(values.password),
        card: {
          last4: paymentMethod.card.last4,
          brand: paymentMethod.card.brand
        }
      }, (err, res) => {
        if (err) {
          toastAlert("danger", err.reason);
          setSubmitting(false);
          this.setState({
            isSubmitting: false
          });
        } else {
          // login user to their new account
          Meteor.loginWithPassword(res, values.password);
        }
      })
    });
  }

  handleVirutalSubmit = () => {
    let hiddenSubmitButton = document.getElementById("signup-form");
    if (hiddenSubmitButton) {
      hiddenSubmitButton.click();
    }
  }

  renderQuotes = () => {
    const { quotes, selectedQuote } = this.state;

    return quotes.map((quote, i) => (
      <QuoteSelect
        selectedQuote={selectedQuote}
        key={`product-${i}`}
        onClick={() => {
          const newSelectedQuote = selectedQuote && selectedQuote._id == quote._id ? null : quote;

          this.setState({
            selectedQuote: newSelectedQuote,
          });
        }}
        {...quote}
      />
    ))
  }

  renderPaymentPlans = () => {
    const { quotes, selectedBillingInterval } = this.state;

    let firstQuote = quotes[0];
    return firstQuote.allowedInstallments
      .sort((a, b) => (a - b))
      .map((months, i) => (
        <PaymentPlanSelect
          selectedBillingInterval={selectedBillingInterval}
          key={`product-${i}`}
          onClick={() => {
            const newSelectedPaymentPlan = selectedBillingInterval && selectedBillingInterval == months ? null : months;

            this.setState({
              selectedBillingInterval: newSelectedPaymentPlan,
            });
          }}
          months={months}
        />
      ))
  }

  render() {
    const { isLoading, quotes, isSubmitting, selectedQuote, selectedBillingInterval } = this.state;

    if (isLoading) {
      return null;
    }

    if (quotes.length == 0) {
      return <NotFound />
    }

    let firstQuote = quotes[0];
    return (
      <Container className='normal-padding'>
        <Row>
          <Col xs='12' className='text-center'>
            <h2 className='mb-4'>{`Select a Plan and Signup`}</h2>
          </Col>
          <Col xs='12'>
            <FormLabel>Select Plan</FormLabel>
            <div className='basic-box'>
              <ListGroup variant='flush'>
                {this.renderQuotes()}
              </ListGroup>
            </div>
          </Col>
          <Col xs='12'>
            <FormLabel>Select Payment Plan</FormLabel>
            <div className='basic-box'>
              <ListGroup variant='flush'>
                {this.renderPaymentPlans()}
              </ListGroup>
            </div>
          </Col>
          <Col xs='12'>
            <AutoForm
              handleSubmit={this.handleSubmit}
              container={{
                styles: {
                  padding: "0px"
                }
              }}
              button={{
                id: "signup-form",
                wrapperStyles: {
                  display: "none"
                }
              }}
              schema={[
                {
                  attributes: {
                    name: "email",
                    type: "email",
                    value: firstQuote.client.email,
                    disabled: true,
                  },
                  key: "email",
                  label: "Email Address"
                },
                {
                  attributes: {
                    name: "password",
                    type: "password",
                    max: 256,
                    placeholder: "Password",
                    required: true,
                    autoComplete: "new-password"
                  },
                  key: "password",
                  label: "Password",
                  validationMethod: Yup.string().required("Required").min(8, 'Minimum of 8 character').max(128, 'Maximum of 128 characters')
                },
                {
                  attributes: {
                    name: "confirmPassword",
                    type: "password",
                    placeholder: "Confirm Password",
                    required: true,
                    autoComplete: "new-password"
                  },
                  key: "confirmPassword",
                  label: "Confirm Password",
                  max: 256,
                  validationMethod: Yup.string().required("Required").oneOf([Yup.ref('password'), null], 'Your passwords do not match')
                }
              ]}
            />
          </Col>
          <Col xs='12'>
            <FormLabel>Payment Information</FormLabel>
            <div className='basic-box p-3'>
              <CardElement
                options={{
                  style: {
                    base: {
                      fontSize: '16px',
                      font: "Ubuntu",
                      '::placeholder': {
                        color: '#aab7c4',
                      },
                    },
                    invalid: {
                      color: '#9e2146',
                    },
                  },
                }}
              />
            </div>
          </Col>
          <Col xs='12'>
            <div className='basic-box pt-3 pb-3'>
              <Container style={{ borderBottom: "1px solid var(--border-color)" }}>
                <Row className='align-items-center pb-3'>
                  <Col xs='auto'>
                    <strong>Total Due Today</strong>
                  </Col>
                  <Col xs='auto' className='ml-auto'>
                    <strong>{selectedQuote && selectedBillingInterval ? centsToDollars(selectedQuote.total / (12 / selectedBillingInterval), selectedQuote.currency.decimalDigits, selectedQuote.currency.symbol) : "—"}</strong>
                  </Col>
                </Row>
              </Container>
              <Container className='pt-3'>
                <Row className='align-items-center'>
                  <Col xs='auto'>
                    <strong>Total Term Amount</strong>
                  </Col>
                  <Col xs='auto' className='ml-auto'>
                    <strong>{selectedQuote && selectedBillingInterval ? centsToDollars(selectedQuote.total, selectedQuote.currency.decimalDigits, selectedQuote.currency.symbol) : "—"}</strong>
                  </Col>
                </Row>
              </Container>
            </div>
          </Col>
          <Col xs='12' className='text-center mt-3'>
            <LoadingButton loading={isSubmitting} size='lg' style={{ width: "200px" }} onClick={this.handleVirutalSubmit}>
              Submit
            </LoadingButton>
          </Col>
        </Row>
      </Container>
    )
  }
}

const SelectQuoteAndCreateAccountWrapped = (props) => (
  <Elements stripe={window.stripePromise}>
    <ElementsConsumer>
      {({ elements, stripe }) => (
        <SelectQuoteAndCreateAccount {...props} elements={elements} stripe={stripe} />
      )}
    </ElementsConsumer>
  </Elements>
);

export default SelectQuoteAndCreateAccountWrapped;