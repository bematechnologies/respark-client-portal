import React from 'react';

import './styles/main.scss';
import 'react-toastify/dist/ReactToastify.css';


import { ToastContainer } from 'react-toastify';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import NotFound from '../../common/NotFound';
import Navigation from '../components/Navigation';
import Signup from '../public/Signup';
import SelectQuoteAndCreateAccount from '../public/SelectQuoteAndCreateAccount';
import CustomRoute from '../components/CustomRoute';
import Home from '../authenticated/Home';
import Account from '../authenticated/Account';
import Login from '../public/Login';

class App extends React.Component {

  render() {
    return (
      <div>
        <Router>
          <Navigation />
          <Switch>
            {/* Authenticated Pages */}
            <CustomRoute authenticated exact path="/home" component={Home} />
            <CustomRoute authenticated exact path="/account" component={Account} />

            {/* Public Pages */}
            <CustomRoute public exact path="/" component={Login} />
            <CustomRoute public exact path="/login" component={Login} />
            <CustomRoute public exact path="/signup/create-account/:quoteGroupId" component={SelectQuoteAndCreateAccount} />
            <CustomRoute public exact path="/signup/:page" component={Signup} />
            <Route public exact path="/signup" render={() => <Redirect to='/signup/get-started' />} />

            {/* Not found */}
            <Route component={NotFound} />
          </Switch>
        </Router>
        <ToastContainer
          className='toast-container'
          position="bottom-right"
          autoClose={2500}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
        />
      </div>
    )
  }
};

export default App;
