import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

const NotFound = () => {
  return (
    <div className='normal-padding'>
      <Container>
        <Row>
          <Col xs='12'>
            <Alert variant='danger' className='p-3'>
              Page Not Found
          </Alert>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default NotFound;