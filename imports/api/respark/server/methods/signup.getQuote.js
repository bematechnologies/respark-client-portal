import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import moment from 'moment';

/*
  Method handler to get a quote
  Used on the /signup/create-account/:quoteId page
*/
const signupGetQuote = (quoteGroupId) => {
  check(quoteGroupId, String);

  try {
    // fetch the quote
    let res = ReSpark.call(`/quotes?quoteGroupId=${quoteGroupId}`, "GET")

    if (res.data.quotes.length == 0) {
      return [];
    }

    let firstQuote = res.data.quotes[0];

    // make sure we can still bind this quoteGroup
    if (firstQuote.status != 'bindable') {
      return [];
    }

    // make sure this quote hasn't expired
    if (moment().isAfter(moment(firstQuote.validUntil))) {
      return [];
    }

    // return paired down quotes here since we don't need expose
    // everything that is available in a quote query
    return res.data.quotes.map(quote => (
      {
        _id: quote._id,
        clientId: quote.clientId,
        limit: quote.limit,
        deductible: quote.deductible,
        termPremium: quote.termPremium,
        allowedInstallments: quote.allowedInstallments,
        total: quote.total,
        currency: {
          symbol: quote.currency.symbol,
          decimalDigits: quote.currency.decimalDigits
        },
        client: {
          name: quote.client.name,
          email: quote.client.email
        }
      }
    ));
  } catch (e) {
    console.log(e.message);
    throw new Meteor.Error("500", e.message);
  }
}

Meteor.methods({
  'signup.getQuote': function (quoteGroupId) {
    return signupGetQuote(quoteGroupId);
  }
})