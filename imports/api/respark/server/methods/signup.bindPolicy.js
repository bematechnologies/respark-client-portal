import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check';

import stripe from '../../../../modules/server/stripe';

import Policies from '../collections/Policies';

import invoiceUpsert from '../events/invoice.upsert';

/*
  Method handler to bind a quote into a policy
*/
const signupBindPolicy = async (data) => {
  check(data, {
    quoteGroupId: String,
    quoteId: String,
    billingInterval: Number,
    paymentMethodId: String,
    password: Object,
    card: {
      last4: String,
      brand: String
    }
  });

  try {
    // bind policy
    let res = ReSpark.call("/quotes/bind", "POST", {
      data: {
        quoteId: data.quoteId,
        billingInterval: data.billingInterval
      }
    });

    let policy = res.data.policy;
    let address = policy.client.addresses[0];
    if (!policy) {
      throw new Meteor.Error("500", "There was an error with your request.");
    }

    // first let's create a customer on Stripe
    let customer = await stripe.customers.create({
      payment_method: data.paymentMethodId,
      invoice_settings: {
        default_payment_method: data.paymentMethodId,
      },
      name: policy.client.name,
      email: policy.client.email,
      phone: policy.client.phone,
      address: {
        line1: address.line1,
        line2: address.line2,
        city: address.city,
        // country: address.country,
        state: address.state,
        postal_code: address.zip
      },
      metadata: {
        resparkPolicyId: policy._id,
        resparkClientId: policy.client._id,
        resparkPolicyNumber: policy.policyNumber
      }
    });

    // create user on our system
    let userId = Accounts.createUser({
      email: policy.client.email,
      password: data.password
    });

    // update fields on user account
    Meteor.users.update(userId, {
      $set: {
        createdAt: (new Date()).toISOString(),
        clientId: policy.client._id,
        name: {
          first: policy.client.firstName,
          last: policy.client.lastName,
          full: policy.client.name,
        },
        role: "user",
        policyId: policy._id,
        customerId: customer.id
      }
    });

    // insert a new policy document
    Policies.insert({
      userId,
      policyId: policy._id,
      quoteId: data.quoteId,
      status: policy.status,
      policyNumber: policy.policyNumber,
      effectiveDate: policy.effectiveDate,
      termEndDate: policy.termEndDate,
      name: policy.name
    });

    // since our webhook for invoice creation will fire before our user is created
    // we go ahead and query for any open invoices and call that event manually
    res = ReSpark.call(`/invoices?status=open&policyId=${policy._id}`, "GET");
    if (res && res.data && res.data.invoices && res.data.invoices[0]) {
      invoiceUpsert(res.data.invoices[0]);
    }

    return policy.client.email;
  } catch (e) {
    console.log(e.message);
    throw new Meteor.Error("500", e.message);
  }
}

Meteor.methods({
  'signup.bindPolicy': async function (data) {
    let email = await signupBindPolicy(data);
    return email;
  }
});