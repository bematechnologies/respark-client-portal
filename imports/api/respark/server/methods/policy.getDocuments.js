import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import jwt from 'jsonwebtoken';
import Policies from '../collections/Policies';

/*
  Method handler to return document tokens for getting policy documents
  We return an array of signed token which the user can then request documents 
  Token are expire in a minute
*/
const policyGetDocuments = (userId, policyId) => {
  check(policyId, String);

  if (!userId) {
    return null;
  }
  
  let policy = Policies.findOne({ policyId });
  if (!policy || policy.userId != userId) {
    return [];
  }

  let user = Meteor.users.findOne(userId);
  let res = ReSpark.call(`/policies/documents/${user.policyId}`, "GET");

  if (!res.data.documents) {
    return [];
  }

  let tokens = [];
  res.data.documents.forEach((document) => {
    tokens.push(jwt.sign(document, Meteor.settings.private.JWT_SIGNING_KEY, {
      expiresIn: "60s"
    }));
  });

  return tokens;
}

Meteor.methods({
  'policy.getDocuments': function(policyId) {
    return policyGetDocuments(this.userId, policyId);
  }
})