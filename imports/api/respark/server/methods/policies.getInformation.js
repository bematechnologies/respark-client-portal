import { Meteor } from 'meteor/meteor';

import Policies from '../collections/Policies';
import Claims from '../collections/Claims';

/*
  Method handler to return policy information
  This is the data return on the /home page
*/
const policiesGetInformation = (userId) => {
  if (!userId) {
    return null;
  }

  let policies = Policies.find({ userId }, { fields: {
    policyId: 1,
    status: 1,
    policyNumber: 1,
    termEndDate: 1,
    name: 1
  }}).fetch();

  let claims = Claims.find({ userId }, { fields: {
    status: 1,
    createdAt: 1
  }}).fetch();

  return {
    policies,
    claims
  };
}

Meteor.methods({
  'policies.getInformation': function() {
    return policiesGetInformation(this.userId);
  }
})