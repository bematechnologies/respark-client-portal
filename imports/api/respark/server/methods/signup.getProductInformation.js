import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

/*
  Method handler to get information for a product
*/
const signupGetProductInformation = () => {
  try {
    let res = ReSpark.call(`/products/${Meteor.settings.private.RESPARK_PRODUCT_ID}`, "GET");
    let data = {
      fields: []
    }

    if (res.data) {
      data.fields = res.data.product.fields || [];
      data.locations = res.data.product.locations.map((location) => {
        if (location.countryAlpha3Code == "USA" && location.type == "state") {
          return {
            display: location.state,
            value: location.stateCode
          }
        }
      });
    }

    return data;
  } catch (e) {
    console.log(e.message);
    throw new Meteor.Error("500", "There was an error with your request");
  }
}

let productCache = null;
Meteor.methods({
  'signup.getProductInformation': function () {
    // note we return from in memory cache reference to save hit times on loading the signup page
    // this can save a significant amount of api calls when the signup page is loaded
    if (productCache) {
      return productCache;
    }

    let data = signupGetProductInformation();
    productCache = data;

    return data;
  },

  // we expose a sloppy clearCache method which allows a client to refresh this
  // we also clear it automatically when we recieve a product update webhook
  'signup.clearCache': function () {
    productCache = null;
  }
});