import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { check, Match } from 'meteor/check';

import moment from 'moment';

/*
  Method handler to create a quote
*/
const signupCreateQuote = (data) => {
  check(data, {
    firstName: String,
    lastName: String,
    emailAddress: String,
    line1: String,
    line2: Match.Maybe(String),
    city: String,
    state: String,
    zip: String,
    additionalFields: Object
  });

  // TODO
  // don't allow users to create quotes more then once
  // some sort of ambiguous message should be returned here?
  let user = Accounts.findUserByEmail(data.emailAddress);
  if (user) {
    throw new Meteor.Error("500", "An account with this email address already exists.");
  }

  if (data.additionalFields.canSwim == 'no') {
    throw new Meteor.Error('500', "We don't insure fish who can't swim!")
  }

  try {
    // fetch the product
    let res = ReSpark.call(`/products/${Meteor.settings.private.RESPARK_PRODUCT_ID}`, "GET");
    let product = res.data.product;

    // create a client
    let clientData = {
      name: `${data.firstName} ${data.lastName}`,
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.emailAddress,
      address: {
        countryAlpha3Code: "USA",
        stateCode: data.state,
        line1: data.line1,
        line2: data.line2,
        city: data.city,
        zip: data.zip
      }
    }

    // create the client on respark
    res = ReSpark.call("/clients", "POST", {
      data: clientData
    });

    let clientId = res.data.client._id;

    // create the quotes
    let quoteData = {
      clientId,
      productId: Meteor.settings.private.RESPARK_PRODUCT_ID,
      effectiveDate: moment().startOf("day").toISOString(),
      termEndDate: moment().startOf("day").add(1, "year").toISOString(),
      plans: product.limits.map(limit => ({ limit: limit.limit, deductible: limit.deductible })),
      metadata: data.additionalFields
    }

    // go ahead and create the quote on respark
    res = ReSpark.call("/quotes", "POST", {
      data: quoteData
    });

    if (res.response?.data?.message) {
      throw new Meteor.Error("401", res.response.data.message);
    }

    let quoteGroupId = null;
    if (res.data && res.data.quotes && res.data.quotes[0]) {
      quoteGroupId = res.data.quotes[0].quoteGroupId;
    }

    return quoteGroupId;
  } catch (e) {
    throw new Meteor.Error("500", e.message);
  }
}

Meteor.methods({
  'signup.createQuote': function (data) {
    return signupCreateQuote(data);
  }
});