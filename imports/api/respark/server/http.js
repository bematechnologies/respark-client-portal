import { WebApp } from 'meteor/webapp';

import jwt from 'jsonwebtoken';
import got from 'got';

WebApp.connectHandlers.use('/documents', (req, res, next) => {
  if (req.method != 'GET') {
    res.writeHead(405);
    res.end();
    return;
  }

  let token = req.headers["x-token"];
  if (!token) {
    res.writeHead(500);
    res.end("No token provided");
    return;
  }

  let data = null;
  try {
    data = jwt.verify(token, Meteor.settings.private.JWT_SIGNING_KEY);
  } catch (e) {
    res.writeHead(500);
    res.end("Invalid token");
    return;
  }

  res.setHeader('Content-Disposition', `inline; filename="${data.title}"`);
  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader("x-title", data.title);
  try {
    got.get(`${Meteor.settings.private.RESPARK_URL}/api/v1/documents/${data.id}`, {
      headers: {
        "x-api-key": Meteor.settings.private.RESPARK_SECRET_KEY
      },
      isStream: true
    }).pipe(res);
  } catch (e) {
    console.error(e);
  }
});