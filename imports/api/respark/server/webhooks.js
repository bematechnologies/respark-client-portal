import { WebApp } from 'meteor/webapp';

import invoiceUpsert from './events/invoice.upsert';
import policyUpsert from './events/policy.upsert';
import claimUpsert from './events/claim.upsert';

WebApp.connectHandlers.use('/webhooks/respark', (req, res, next) => {
  if (req.body && req.body.action) {
    Meteor.defer(() => {
      switch (req.body.action) {
        case "invoices.created":
        case "invoices.updated":
          invoiceUpsert(req.body.data);
          break;

        case "policy.updated":
        case "policy.created":
          policyUpsert(req.body.data);
          break;

        case "claim.updated":
        case "claim.created":
          claimUpsert(req.body.data);
          break;

        case "product.updated":
          Meteor.call("signup.clearCache");
          break;

        // TODO
        // client update events
        // https://docs.respark.insure/#tag/Events/paths/~1events~1client.updated/post

        default:
          break;
      }
    });
  }

  res.writeHead(200);
  res.end(`Webhook recieved`);
});