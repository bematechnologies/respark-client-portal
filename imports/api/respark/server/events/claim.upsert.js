import Policies from "../collections/Policies";
import Claims from "../collections/Claims";

const claimUpsert = (claim) => {
  const user = Meteor.users.findOne({ clientId: claim.client._id });
  if (!user) {
    return;
  }

  let policy = Policies.findOne({ policyId: claim.policy._id });
  if (!policy) {
    return;
  }

  let existingClaim = Claims.findOne({ claimId: claim._id });
  if (existingClaim) {
    console.log(`Claim updated for ${user.name.full} for claim ${claim._id}`);

    Claims.update(existingClaim._id, {
      $set: {
        description: claim.description,
        status: claim.status
      }
    });
  } else {
    console.log(`New claim inserted for ${user.name.full} for claim ${claim._id}`);

    Claims.insert({
      claimId: claim._id,
      policyId: claim.policy._id,
      description: claim.description,
      status: claim.status,
      userId: user._id
    });
  }
}

export default claimUpsert;