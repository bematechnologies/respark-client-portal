import Policies from "../collections/Policies";

const policiesUpsert = (policy) => {
  const user = Meteor.users.findOne({ clientId: policy.client._id });
  if (!user) {
    return;
  }

  let existingPolicy = Policies.findOne({ policyId: policy._id });
  if (existingPolicy) {
    console.log(`Policy updated for ${user.name.full} for policy ${policy._id}`);

    Policies.update(existingPolicy._id, {
      $set: {
        status: policy.status,
        policyNumber: policy.policyNumber,
        termEndDate: policy.termEndDate,
        name: policy.name
      }
    });
  } else {
    console.log(`New policy inserted for ${user.name.full} for policy ${policy._id}`);

    Policies.insert({
      userId: user._id,
      policyId: policy._id,
      quoteId: policy.quoteId,
      status: policy.status,
      policyNumber: policy.policyNumber,
      effectiveDate: policy.effectiveDate,
      termEndDate: policy.termEndDate,
      name: policy.name
    });
  }
}

export default policiesUpsert;