import stripe from "../../../../modules/server/stripe";

import Policies from "../collections/Policies";

/*
  Event handler for invoices.created and invoices.updated
  Handles creation and management of invoices with Stripe
*/
const invoiceUpsert = async (invoice) => {
  try {
    
    // not worried about draft invoices for now
    if (invoice.status == 'draft') {
      return;
    }

    let policy = Policies.findOne({ policyId: invoice.policyId });
    if (!policy) {
      return;
    }

    const user = Meteor.users.findOne(policy.userId);
    if (!user) {
      return;
    }

    // let's make sure we don't have alreading existing invoice
    // a client shouldn't have more then 20 open invoices?
    // we can't do lookups with metadata so we have to do it this way
    let invoices = await stripe.invoices.list({
      limit: 20,
      customer: user.customerId,
      status: "open",
    });

    // if we found an invoice let's leave it alone until unless it is voided or marked uncollectible
    let currentInvoice = invoices.data.find((stripeInvoice) => stripeInvoice.metadata.resparkInvoiceId == invoice._id);
    if (currentInvoice) {

      // void the current invoice if that action occurs
      if (invoice.status == "void") {
        stripe.invoices.voidInvoice(currentInvoice.id);
      }

      // mark invoice as uncollection if that action occurs
      if (invoice.status == "uncollectible") {
        stripe.invoices.markUncollectible(currentInvoice.id);
      }

      return;
    }

    // let's create an invoice
    // this is charged automatically to a clients default payment method
    // we also finalize it right away so it can be charged
    if (invoice.status == "open") {

      // create the invoice item
      // if you need to invoice on multiple items extend this here with more invoice items
      await stripe.invoiceItems.create({
        customer: user.customerId,
        amount: invoice.total,
        currency: invoice.currency.code ? invoice.currency.code.toLowerCase() : "usd",
        description: 'Premium payment',
      });

      // create the invoice
      // we auto_advance to perform automatic collection https://stripe.com/docs/api/invoices/create#create_invoice-auto_advance
      const newInvoice = await stripe.invoices.create({
        customer: user.customerId,
        auto_advance: true,
        metadata: {
          resparkInvoiceId: invoice._id,
          resparkPolicyId: policy.policyId
        }
      });

      // finally let's go ahead and attempt to pay the invoice
      await stripe.invoices.pay(newInvoice.id);

      console.log(`New invoice created for ${user.name.full} for policy ${policy.policyId}`)
    }
  } catch(e) {
    console.error(e);
  }
}

export default invoiceUpsert;