import SimpleSchema from 'simpl-schema';

// schema definitions for policies
// extend as needed

const Policies = new Mongo.Collection('policies');

let schema = new SimpleSchema({
  createdAt: {
    type: String,
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  policyId: String,
  quoteId: String,
  userId: String,
  status: String,
  policyNumber: String,
  effectiveDate: String,
  termEndDate: String,
  name: String
});

Policies.attachSchema(schema);

export default Policies;
