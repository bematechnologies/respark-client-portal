import SimpleSchema from 'simpl-schema';

// schema definitions for claims
// extend as needed

const Claims = new Mongo.Collection('claims');

let schema = new SimpleSchema({
  createdAt: {
    type: String,
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  claimId: String,
  policyId: String,
  description: String,
  status: String,
  userId: String
});

Claims.attachSchema(schema);

export default Claims;
