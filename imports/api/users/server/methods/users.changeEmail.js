import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check';

/*
  Method handler to change a users email
  Note we rely on Meteor's helper method for this
*/
const usersChangeEmail = (userId, newEmail) => {
  check(newEmail, String);

  if (!userId) {
    return;
  }

  let user = Meteor.users.findOne(userId);
  Accounts.removeEmail(userId, user.emails[0].address);
  Accounts.addEmail(userId, newEmail);

  // update the email address on respark
  let res = ReSpark.call(`/clients/${user.clientId}`, "GET");
  if (res.data && res.data.client) {
    const client = res.data.client;

    ReSpark.call(`/clients/${user.clientId}`, "POST", {
      data: {
        name: client.name,
        firstName: client.firstName,
        lastName: client.lastName,
        email: newEmail,
        address: client.defaultAddress
      }
    });
  }
}

Meteor.methods({
  'users.changeEmail': function (newEmail) {
    usersChangeEmail(this.userId, newEmail);
  }
})