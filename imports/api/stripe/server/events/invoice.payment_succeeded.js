/*
  Event handler for invoices.payment_succeeded
  We go ahead and update the invoice on ReSpark to mark it paid
*/
const invoicePaymentSucceeded = (invoice) => {
  if (invoice.metadata.resparkInvoiceId) {
    console.log(`Invoice ${invoice.metadata.resparkInvoiceId} marked as paid.`);

    ReSpark.call("/invoices", "POST", {
      data: {
        _id: invoice.metadata.resparkInvoiceId,
        status: "paid"
      }
    });
  }
}

export default invoicePaymentSucceeded;