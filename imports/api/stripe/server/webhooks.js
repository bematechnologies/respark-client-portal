import { WebApp } from 'meteor/webapp';
import invoicePaymentSucceeded from './events/invoice.payment_succeeded';

// endpoint handler for receiving webhooks from stripe
WebApp.connectHandlers.use('/webhooks/stripe', (req, res, next) => {
  // TODO
  // please note webhook signing tokens should be validated
  // i didn't take the time to set that up

  if (req && req.body) {
    Meteor.defer(() => {
      let body = req.body;
      let type = body.type;
      let data = body.data.object;

      if (!type) {
        console.error("Type not found in Stripe webhook request");
        return
      }

      if (!data) {
        console.error(`No data body found in stripe webhook for ${type}`);
      }

      switch (type) {
        case "invoice.payment_succeeded":
          invoicePaymentSucceeded(data);
          break;
        default:
          break;
      }
    });
  }

  res.writeHead(200);
  res.end(`Webhook recieved`);
});