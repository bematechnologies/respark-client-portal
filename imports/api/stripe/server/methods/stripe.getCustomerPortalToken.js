import { Meteor } from 'meteor/meteor';
import stripe from '../../../../modules/server/stripe';

/*
  Method handler to update the default payment method for a stripe customer
*/
const stripeGetCustomerPortalToken = async (userId) => {
  const user = Meteor.users.findOne(userId);

  const session = await stripe.billingPortal.sessions.create({
    customer: user.customerId,

    // stripe doesn't support localhost in the redirect
    // so we're go ahead to the client demo hosted by respark if it is on localhost
    return_url: Meteor.absoluteUrl().includes("localhost") ? "https://client-demo.respark.insure/account" : Meteor.absoluteUrl("/account"),
  });

  return session.url;
}

Meteor.methods({
  'stripe.getCustomerPortalToken': async function() {
    return await stripeGetCustomerPortalToken(this.userId);
  }
});