import { toast } from 'react-toastify';

const toastAlert = (type, message, timeout) => {
  if (!message && !type) {
    return;
  }

  if (!message && type) {
    toast['success'](type, {
      position: "bottom-right",
      autoClose: timeout || 2500,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  } else {
    if (type == 'danger') {
      type = 'error';
    }

    toast[type](message, {
      position: "bottom-center",
      autoClose: timeout || 2500,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  }
}

export default toastAlert;