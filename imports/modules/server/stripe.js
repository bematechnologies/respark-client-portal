import Stripe from 'stripe';

const stripe = Stripe(Meteor.settings.private.STRIPE_SECRET_KEY);

export default stripe;