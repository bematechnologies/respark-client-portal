export default (cents, decimalDigits, symbol) => {
  if (typeof(decimalDigits) != 'number') {
    decimalDigits = 2;
  }

  if (!symbol) {
    symbol = '$'
  }

  return `${symbol}${(cents / Math.pow(10, decimalDigits)).toFixed(decimalDigits).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`;
}
